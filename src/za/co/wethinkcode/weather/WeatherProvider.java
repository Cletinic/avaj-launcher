package za.co.wethinkcode.weather;

import java.util.Random;

// Singleton
public class WeatherProvider
{

    private static WeatherProvider weatherProvider = new WeatherProvider();
    private static String[] weather = {"sun", "fog", "snow", "rain"};

    private WeatherProvider() {}

    public static WeatherProvider getProvider()
    {
        return weatherProvider;
    }
    public String getCurrentWeather(Coordinates coordinates)
    {
//
//        // create random object
//        Random random = new Random();
//
//        // Returns between 0 (inclusive) and n - in this case weather.length - (exclusive).
//        int ranNum = random.nextInt(weather.length);

        int num = ((coordinates.getHeight() + coordinates.getLatitude() + coordinates.getLongitude()) % weather.length);

        return weather[num];
    }
}
