package za.co.wethinkcode;

import za.co.wethinkcode.simulator.WeatherTower;
import za.co.wethinkcode.simulator.vehicles.AircraftFactory;
import za.co.wethinkcode.simulator.vehicles.Flyable;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

class AircraftException extends Exception
{
    public AircraftException() { super(); }
}

public class Main
{
    private static WeatherTower weatherTower;
    private static List<Flyable> flyableList = new ArrayList<Flyable>();

    public static void main(String[] argv) throws InterruptedException
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(argv[0]));
            String line = reader.readLine();

            if (line != null)
            {
                Logger.makeFile();
                weatherTower = new WeatherTower();

                int simulations = Integer.parseInt(line.split(" ")[0]);
                if (simulations < 0)
                {
                    System.out.println("Invalid simulations count" + simulations);
                    System.exit(1);
                }

                while ((line = reader.readLine()) != null)
                {

                    Flyable flyable = AircraftFactory.newAircraft((line.split(" ")[0]), (line.split(" ")[1]), (Integer.parseInt(line.split(" ")[2])), (Integer.parseInt(line.split(" ")[3])), (Integer.parseInt(line.split(" ")[4])));
                    //System.out.println(flyable);

                    if (flyable != null)
                        flyableList.add(flyable);
                }

                if (flyableList.size() == 0)
                {
                    Logger.fileDeleteBool =  true;
                    throw new AircraftException();
                }

                Logger.writeToFile("_____________________________________________ Register All Air Crafts ______________________________________________\n");

                for (Flyable flyable : flyableList) {
                    flyable.registerTower(weatherTower);
                }

                Logger.writeToFile("\n_____________________________________________ Start of Simulation Runs _____________________________________________\n");

                int i = 0;
                while (i < simulations)
                {
                    Logger.writeToFile("\n    Simulation: " + (i + 1) + " / " + simulations + "\n");
                    weatherTower.changeWeather();
                    i++;
                }

                Logger.writeToFile("\n______________________________________________ End of Simulation Runs ______________________________________________");
            }

            reader.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Could not read file");
        }
        catch (IOException e)
        {
            System.out.println("There was an error reading the file");
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println("File not specified / Or failed to read");
        }
        catch (AircraftException e)
        {
            System.out.println("Aircraft cannot register to Tower");
        }
        finally
        {
            Logger.closeFile();
        }
        return;
    }
}
