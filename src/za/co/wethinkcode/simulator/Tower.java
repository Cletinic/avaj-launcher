package za.co.wethinkcode.simulator;

import za.co.wethinkcode.simulator.vehicles.Flyable;

import java.util.ArrayList;
import java.util.List;

public abstract class Tower
{

    private List<Flyable> observers = new ArrayList<Flyable>();

    public void register(Flyable flyable)
    {
        if (!observers.contains(flyable))
            observers.add(flyable);

        return;
    }

    public void unregister(Flyable flyable)
    {
        observers.remove(flyable);
    }

    protected void conditionsChanged()
    {
        for (int i = 0; i < observers.size(); i++)
        {
            Flyable craft = observers.get(i);

            craft.updateConditions();
            // check if landed?
        }

        return;
    }
}
