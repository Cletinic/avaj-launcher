package za.co.wethinkcode.simulator.vehicles;

import za.co.wethinkcode.Logger;
import za.co.wethinkcode.simulator.WeatherTower;
import za.co.wethinkcode.weather.Coordinates;

import java.util.HashMap;

public class Balloon extends Aircraft implements Flyable
{
    private WeatherTower weatherTower;

    public Balloon(String name, Coordinates coordinates)
    {
        super(name, coordinates);

        if (this.coordinates.getHeight() >= 100)
            this.coordinates = new Coordinates(coordinates.getLongitude(), coordinates.getLatitude(), 100);
    }

    @Override
    public void updateConditions()
    {
        HashMap<String, String> map = new HashMap<String, String>();

        map.put("sun", "Let's enjoy the good weather and take some pics.");
        map.put("rain", "Damn you rain! You messed up my balloon.");
        map.put("fog", "Damn, can't see a thing!");
        map.put("snow", "It's snowing. We're gonna crash.");

        String weather = weatherTower.getWeather(this.coordinates);

        int height = this.coordinates.getHeight();
        int longitude = this.coordinates.getLongitude();
        int latitude = this.coordinates.getLatitude();

        switch(weather.toLowerCase())
        {

            case "sun":
                this.coordinates = new Coordinates((longitude + 2), (latitude), (height + 4));
                break;
            case "rain":
                this.coordinates = new Coordinates((longitude), (latitude), (height - 5));
                break;
            case "fog":
                this.coordinates = new Coordinates((longitude), (latitude), (height - 3));
                break;
            case "snow":
                this.coordinates = new Coordinates((longitude), (latitude), (height - 15));
                break;
            //default:
            // code block
        }

        if (this.coordinates.getHeight() > 100)
            this.coordinates = new Coordinates((longitude), (latitude), (100));
        if (this.coordinates.getHeight() <= 0)
            this.coordinates = new Coordinates((longitude), (latitude), (0));

        //Print to file
        String str = "Balloon " + this.name + "(" + this.id + ")       : "  + map.get(weather.toLowerCase()) + " //Coordinates: H: " + this.coordinates.getHeight() + " Long: " + this.coordinates.getLongitude() + " Lat: " + this.coordinates.getLatitude();
        Logger.writeToFile(str);

        if (this.coordinates.getHeight() == 0)
        {


            str = "Balloon " + this.name + "(" + this.id + ")       : Landing" + " // Coordinates: H: " + this.coordinates.getHeight() + " Long: " + this.coordinates.getLongitude() + " Lat: " + this.coordinates.getLatitude();
            Logger.writeToFile(str);

            Logger.writeToFile("\n---------------------------------------------- Unregister Air Craft ------------------------------------------------");


            str = "Tower says          : Balloon " + this.name + "(" + this.id + ")" +" unregistered from weather tower.";
            Logger.writeToFile(str);
            this.weatherTower.unregister(this);

            Logger.writeToFile("--------------------------------------------------------------------------------------------------------------------\n");
        }

        return;
    }

    @Override
    public void registerTower(WeatherTower weatherTower)
    {
        this.weatherTower = weatherTower;
        this.weatherTower.register(this);
        String str = "Tower says          : Balloon #" +  this.name + "(" + this.id + ")"  + "registered to weather tower.";
        Logger.writeToFile(str);

        return;
    }
}