package za.co.wethinkcode.simulator.vehicles;

import za.co.wethinkcode.Logger;
import za.co.wethinkcode.simulator.WeatherTower;
import za.co.wethinkcode.weather.Coordinates;

import java.util.HashMap;

public class Helicopter extends Aircraft implements Flyable
{
    private WeatherTower weatherTower;

    public Helicopter(String name, Coordinates coordinates)
    {
        super(name, coordinates);

        if (this.coordinates.getHeight() > 100)
            this.coordinates = new Coordinates(coordinates.getLongitude(), coordinates.getLatitude(), 100);

    }

    @Override
    public void updateConditions()
    {

        HashMap<String, String> map = new HashMap<String, String>();

        map.put("sun", "This is hot.");
        map.put("rain", "Damn you rain!");
        map.put("fog", "Damn, can't see a thing!");
        map.put("snow", "My rotor is going to freeze!");

        String weather = weatherTower.getWeather(this.coordinates);

        int height = this.coordinates.getHeight();
        int longitude = this.coordinates.getLongitude();
        int latitude = this.coordinates.getLatitude();

        //System.out.println("helicopter "+ name + " Height:" + coordinates.getHeight() + " Latitude:" + coordinates.getLatitude() + " Longitude" + coordinates.getLongitude());

        switch(weather.toLowerCase())
        {
            case "sun":
                this.coordinates = new Coordinates ((longitude + 10), (latitude), (height + 2));
                break;
            case "rain":
                this.coordinates = new Coordinates ((longitude + 5), (latitude), (height));
                break;
            case "fog":
                this.coordinates = new Coordinates ((longitude + 1), (latitude), (height));
                break;
            case "snow":
                this.coordinates = new Coordinates ((longitude), (latitude), (height - 12));
                break;
            //default:
            //    code block
        }
        //System.out.println("helicopter "+ name + " Height:" + coordinates.getHeight() + " Latitude:" + coordinates.getLatitude() + " Longitude" + coordinates.getLongitude());

        if (this.coordinates.getHeight() > 100)
            this.coordinates = new Coordinates((longitude), (latitude), (100));
        if (this.coordinates.getHeight() <= 0)
            this.coordinates = new Coordinates((longitude), (latitude), (0));

        //Print to file
        String str = "Helicopter " + this.name + "(" + this.id + ")    : "  + map.get(weather.toLowerCase()) + " //Coordinates: H: " + this.coordinates.getHeight() + " Long: " + this.coordinates.getLongitude() + " Lat: " + this.coordinates.getLatitude();
        Logger.writeToFile(str);

        if (this.coordinates.getHeight() == 0)
        {
            str = "Helicopter " + this.name + "(" + this.id + ")    : Landing" + " // Coordinates: H: " + this.coordinates.getHeight() + " Long: " + this.coordinates.getLongitude() + " Lat: " + this.coordinates.getLatitude();
            Logger.writeToFile(str);

            Logger.writeToFile("\n---------------------------------------------- Unregister Air Craft ------------------------------------------------");

            str = "Tower says          : Helicopter " + this.name + "(" + this.id + ")" +" unregistered from weather tower.";
            Logger.writeToFile(str);
            this.weatherTower.unregister(this);

            Logger.writeToFile("--------------------------------------------------------------------------------------------------------------------\n");

        }

        return;
    }

    @Override
    public void registerTower(WeatherTower weatherTower)
    {
        this.weatherTower = weatherTower;
        this.weatherTower.register(this);
        String str = "Tower says          : Helicopter #" +  this.name + "(" + this.id + ")"  + "registered to weather tower.";
        Logger.writeToFile(str);

        return;
    }
}

