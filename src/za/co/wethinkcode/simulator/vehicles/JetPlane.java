package za.co.wethinkcode.simulator.vehicles;

import za.co.wethinkcode.Logger;
import za.co.wethinkcode.simulator.WeatherTower;
import za.co.wethinkcode.weather.Coordinates;
import za.co.wethinkcode.weather.WeatherProvider;

import java.util.HashMap;

public class JetPlane extends Aircraft implements Flyable
{
    private WeatherTower weatherTower;

    public JetPlane(String name, Coordinates coordinates)
    {
        super(name, coordinates);

        if (this.coordinates.getHeight() >= 100)
            this.coordinates = new Coordinates(coordinates.getLongitude(), coordinates.getLatitude(), 100);

    }

    @Override
    public void updateConditions()
    {
        HashMap<String, String> map = new HashMap<String, String>();

        map.put("sun", "Whew! It is boiling!");
        map.put("rain", "It's raining. Better watch out for lighting.");
        map.put("fog", "The fog is killing me. I can't even see the ground!");
        map.put("snow", "OMG! Winter is coming!");

        String weather = weatherTower.getWeather(this.coordinates);

        int height = this.coordinates.getHeight();
        int longitude = this.coordinates.getLongitude();
        int latitude = this.coordinates.getLatitude();

        switch(weather.toLowerCase())
        {
            case "sun":
                this.coordinates = new Coordinates ((longitude), (latitude + 10), (height + 2));
                break;
            case "rain":
                this.coordinates = new Coordinates ((longitude), (latitude + 5), height);
                break;
            case "fog":
                this.coordinates = new Coordinates ((longitude), (latitude + 1), (height));
                break;
            case "snow":
                this.coordinates = new Coordinates ((longitude), (latitude), (height - 7));
                break;
//            default:
//                 code block
        }
        //System.out.println("jetplane "+ name + " Height:" + coordinates.getHeight() + " Latitude:" + coordinates.getLatitude() + " Longitude" + coordinates.getLongitude());

        if (this.coordinates.getHeight() > 100)
            this.coordinates = new Coordinates((longitude), (latitude), (100));
        if (this.coordinates.getHeight() <= 0)
            this.coordinates = new Coordinates((longitude), (latitude), (0));

        //Print to file
        String str = "JetPlane " + this.name + "(" + this.id + ")      : "  + map.get(weather.toLowerCase()) + " //Coordinates: H: " + this.coordinates.getHeight() + " Long: " + this.coordinates.getLongitude() + " Lat: " + this.coordinates.getLatitude();
        Logger.writeToFile(str);

        if (this.coordinates.getHeight() == 0)
        {
            str = "JetPlane " + this.name + "(" + this.id + ")      : Landing" + " // Coordinates: H: " + this.coordinates.getHeight() + " Long: " + this.coordinates.getLongitude() + " Lat: " + this.coordinates.getLatitude();
            Logger.writeToFile(str);

            Logger.writeToFile("\n---------------------------------------------- Unregister Air Craft -----------------------------------------------");

            str = "Tower says          : JetPlane " + this.name + "(" + this.id + ")" +" unregistered from weather tower.";
            Logger.writeToFile(str);
            this.weatherTower.unregister(this);

            Logger.writeToFile("--------------------------------------------------------------------------------------------------------------------\n");
        }

        return;
    }

    @Override
    public void registerTower(WeatherTower weatherTower)
    {
        this.weatherTower = weatherTower;
        this.weatherTower.register(this);
        String str = "Tower says          : JetPlane #" +  this.name + "(" + this.id + ")"  + "registered to weather tower.";
        Logger.writeToFile(str);

        return;
    }
}



