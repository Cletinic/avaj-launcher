package za.co.wethinkcode.simulator.vehicles;

import za.co.wethinkcode.simulator.WeatherTower;

public interface Flyable
{
    public void updateConditions();
    public void registerTower(WeatherTower weatherTower);
}
