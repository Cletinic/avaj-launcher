package za.co.wethinkcode.simulator.vehicles;

import za.co.wethinkcode.weather.Coordinates;

public abstract class AircraftFactory
{
    // type is the vehicle type, name is the id
    public static Flyable  newAircraft(String type, String name, int longitude, int latitude, int height)
    {
        Coordinates coordinates = new Coordinates (longitude, latitude, height);
        switch(type.toLowerCase())
        {
            case "baloon":
                return new Baloon(name, coordinates);
                //break;
            case "balloon":
                return new Balloon(name, coordinates);
                //break;
            case "helicopter":
                return new Helicopter(name, coordinates);
                //break;
            case "jetplane":
                return new JetPlane(name, coordinates);
                //break;
            default:
                return null;
        }
    }
}
