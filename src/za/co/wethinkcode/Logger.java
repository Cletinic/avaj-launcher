package za.co.wethinkcode;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {

    private static File file = null;
    //        private static FileWriter fileWriter = null;
    private static BufferedWriter writer = null;
    public static boolean fileDeleteBool = false;

    public Logger() { }

    public static void makeFile()
    {
        try
        {
            file = new File("simulation.txt");
            file.delete();

            // creates the file
            file.createNewFile();

            //creates a FileWriter Object
            writer = new BufferedWriter(new FileWriter(file, true));
        }
        catch (IOException e)
        {
            System.out.println("There was an error while creating the file");
        }

        return;
    }

    public static void writeToFile(String str) {

        try
        {
            writer.write(str);
            writer.newLine();
        }
        catch (IOException e)
        {
            System.out.println("There was an error while writing to the file");
        }

        return ;
    }


    public static void closeFile()
    {
        try
        {
            if (writer != null)
                writer.close();
            if (file != null && fileDeleteBool)
                file.delete();
        }
        catch (IOException e)
        {
            System.out.println("There was an error while closing the file");
        }

        return;
    }
}
